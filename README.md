# ACRCloudConverter

This is a basic app for Another Call Recorder (ACR) to rename your cloud backup files in required format for program

### Prerequisites

* .Net Framework 4.0 Client Profile
* A Microsoft Operation System

### Installing

You can compile and run it from the source code or you can use ACRCloudConverter.exe directly.

### Run

Copy ACRCloudConverter.exe to the folder where the m4a, 3gp, mp4, mp3, amr, flac, ogg and wav extensions files are located and run it. (Recommended by Command Prompt)
It will automatically rename files with extension m4a, 3gp, mp4, mp3, amr, flac, ogg and wav in the folder.

**The program will assume these records as incoming because the ACR does not distinguish between incoming / outgoing calls in the old backup file name format.**

## Screenshots

### Before

![Before](before.png)

### Process

![Process](process.png)

### After

![After](after.png)

## Authors

* **Muzaffer AKYIL** - *Initial work* - [victorious](https://muzaffer.akyil.net)