﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace ACRCloudConverter
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Clear();
            Console.WriteLine("Another Call Recorder (ACR) Cloud Backup Filename Format Converter");
            Console.WriteLine("Developed by Muzaffer AKYIL (Victorious) - https://muzaffer.akyil.net");
            Console.WriteLine("Source Code Repo : https://bitbucket.org/qtechnics/acrcloudconverter");
            Console.WriteLine();
            string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
            string directory = Path.GetDirectoryName(path);
            List<string> exts = new List<string> { ".m4a", ".3gp", ".mp3", ".mp4", ".amr", ".flac", ".ogg", ".wav" };
            DirectoryInfo d = new DirectoryInfo(directory);
            List<FileInfo> files = d.GetFiles("*.*", SearchOption.AllDirectories).Where(s => exts.Contains(s.Extension.ToLower())).ToList();

            if (files.Count == 0)
            {
                Console.WriteLine("There is no file with m4a, 3gp, mp3, mp4, amr, flac, ogg, wav extensions.");
                Environment.Exit(0);
            }

            Console.WriteLine("{0} call record were found.", files.Count);
            Console.WriteLine("Press any key to continue... ESC to exit application.");
            var ret = Console.ReadKey();
            if (ret.Key == ConsoleKey.Escape)
                Environment.Exit(0);

            foreach (FileInfo file in files)
            {
                string newfile = "";
                Console.Write(file.Name);
                try
                {
                    newfile = string.Format("{0}d{1}p{2}{3}",
    Regex.Match(file.Name, @"_\[(0|1)\]").Success ? Regex.Match(file.Name, @"\[(0|1)\]").Groups[1].Value : "0",
    Regex.Match(file.Name, @"(\d{4}_\d{2}_\d{2}_\d{2}_\d{2}_\d{2})").Groups[1].Value.Replace("_", string.Empty),
    Regex.Matches(file.Name, @"\d+").Cast<Match>().Where(w => w.Value.Length > 2 && !(long.Parse(w.Value) >= 2000 && long.Parse(w.Value) <= DateTime.Today.Year)).FirstOrDefault(),
    file.Extension);
                    Console.Write(" --> " + newfile + ": ");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(" --> ERROR : " + ex.Message);
                }

                try
                {
                    File.Move(file.FullName, directory + @"\" + newfile);
                    Console.WriteLine("OK");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("ERROR : " + ex.Message);
                }
            }

            Console.WriteLine("Operation is complated.");
            Console.ReadLine();
        }
    }
}
